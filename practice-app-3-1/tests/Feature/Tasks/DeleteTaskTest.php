<?php

namespace Tests\Feature\Tasks;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;

class DeleteTaskTest extends TestCase
{
    /**
     * @test
     */
    public function unauthenticated_cannot_delete_a_task(): void
    {
        $task = Task::factory()->create();
        $response = $this->delete(route('tasks.destroy', $task->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_cannot_delete_a_task_with_invalid_id(): void
    {
        $this->actingAs(User::factory()->create());
        $id = -1;
        $numberOfTaskBefore = Task::count();
        $response = $this->delete(route('tasks.destroy', $id));
        $numberOfTaskAfter = Task::count();
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertEquals($numberOfTaskBefore, $numberOfTaskAfter);
    }

    /** @test */
    public function authenticated_can_delete_a_task_with_valid_id(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $numberOfTaskBefore = Task::count();
        $response = $this->delete(route('tasks.destroy', $task->id));
        $numberOfTaskAfter = Task::count();
        $response->assertStatus(302);
        $response->assertRedirect(route('tasks.index'));
        $this->assertEquals($numberOfTaskBefore, $numberOfTaskAfter + 1);
    }
}
