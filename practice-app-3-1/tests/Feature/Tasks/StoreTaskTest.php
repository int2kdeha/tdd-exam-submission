<?php

namespace Tests\Feature\Tasks;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;

class StoreTaskTest extends TestCase
{
    /**
     * @test
     */
    public function unauthenticated_cannot_store_task(): void
    {
        $task = Task::factory()->make();
        $response = $this->post(route('tasks.store'), $task->toArray());
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_cannot_store_task_with_invalid_name(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make();
        $task->name = '';
        $response = $this->post(route('tasks.store'), $task->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect('/');
    }

    /**
     * @test
     */
    public function authenticated_cannot_store_task_with_invalid_body(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make();
        $task->body = '';
        $response = $this->post(route('tasks.store'), $task->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors('body');
        $response->assertRedirect('/');
    }

    /**
     * @test
     */
    public function authenticated_cannot_store_task_with_invalid_body_and_name(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make();
        $task->body = '';
        $task->name = '';
        $response = $this->post(route('tasks.store'), $task->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['body', 'name']);
        $response->assertRedirect('/');
    }

    /** @test */
    public function authenticated_can_store_task_with_valid_body_and_name()
    {
        $this->actingAs(User::factory()->create());
        $countTaskBefore = Task::count();
        $task = Task::factory()->make();
        $task->body = 'ok man';
        $task->name = 'this is new';
        $response = $this->post(route('tasks.store'), $task->toArray());
        $response->assertStatus(302);
        $response->assertRedirect(route('tasks.index'));
        $countTaskAfter = Task::count();
        $this->assertEquals($countTaskBefore + 1, $countTaskAfter);
    }

    /** @test */
    public function new_task_is_always_old_max_id_plus_one()
    {
        $oldMaxId = Task::max('id');
        $task = Task::factory()->create();
        $newMaxId = Task::max('id');
        $this->assertEquals($oldMaxId + 1, $newMaxId);
    }
}
